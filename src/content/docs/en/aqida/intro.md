---
title: Intro for the book of Aqida

description: A guide in my new Starlight docs site.
sidebar:
  # Set a custom label for the link
#   label: Custom sidebar label
  # Set a custom order for the link (lower numbers are displayed higher up)
  order: 1
  # Add a badge to the link
  badge:
    text: intro
    variant: tip
---

Intro for the book of Aqida
